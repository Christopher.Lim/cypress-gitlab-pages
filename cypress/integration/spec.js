// cypress/integration/spec.js
/// <reference types="cypress" />

describe('Cypress Gitlab Pages Test Suite', () => {
  before('Running all beforeAll methods.',() => {
    cy.log(`Launching ${Cypress.config().baseUrl}`);
    cy.visit('/');
  });

  it('should have the correct url path', () => {
    cy.location().should((loc) => {
      expect(loc.pathname).to.eq('/cypress-gitlab-pages/');
    })
  });

  it('should have the correct page content', () => {
      cy
        .contains('.site-name', 'Cypress Gitlab Pages')
        .get('#cypress-gitlab-pages').should('contain.text', 'Cypress Gitlab Pages')
        .get('em').should('contain.text', 'This is a POC to Test Gitlab Pages using Cypress')
        .get('.theme-default-content > :nth-child(3)').should('contain.text', 'For more information about Gitlab Pages and packages used for this repo please follow the link below')
        .get('ul > :nth-child(1) > a').should('contain.text', 'Gitlab Pages')
        .get('ul > :nth-child(2) > a').should('contain.text', 'Cypress')
        .get('ul > :nth-child(3) > a').should('contain.text', 'VuePress')
        .get('ul > :nth-child(4) > a').should('contain.text', 'Start Serve and Test')
  });

});