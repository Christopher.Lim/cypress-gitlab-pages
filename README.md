# cypress-gitlab-pages

*This is a POC to Test Gitlab Pages using Cypress*

### Installation
```
git clone git@gitlab.com:Christopher.Lim/cypress-gitlab-pages.git

npm install
```
### Running the Project
```
npm run build
npm run start
```

### How to run Cypress Test Runner
```
npm run e2e:local
```