# Cypress Gitlab Pages

*This is a POC to Test Gitlab Pages using Cypress*

For more information about Gitlab Pages and packages used for this repo please follow the link below:

- [Gitlab Pages](https://docs.gitlab.com/ee/user/project/pages/)
- [Cypress](https://www.cypress.io/)
- [VuePress](https://vuepress.vuejs.org/guide/getting-started.html#manual-installation)
- [Start Serve and Test](https://www.npmjs.com/package/start-server-and-test)

