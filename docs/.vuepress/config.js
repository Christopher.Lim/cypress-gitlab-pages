module.exports = {
  title: 'Cypress Gitlab Pages',
  description: 'This is a Static Website that has been deployed to Gitlab pages and tested using Cypress',
  base: '/cypress-gitlab-pages/',
  dest: 'public'
}